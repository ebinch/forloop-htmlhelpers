﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

namespace Forloop.HtmlHelpers
{
    /// <summary>
    /// A context in which to add references to style files and blocks of style
    /// to be rendered to the view at a later point.
    /// </summary>
    public class StyleContext : IDisposable
    {
        internal const string StyleContextItem = "StyleContext";
        internal const string StyleContextItems = "StyleContexts";
        private static HttpContextBase _context;
        private readonly HttpContextBase _httpContext;
        private readonly TextWriter _writer;
        private readonly bool _isAjaxRequest;
        internal readonly IList<string> _styleBlocks = new List<string>();
        internal readonly HashSet<string> _styleFiles = new HashSet<string>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="StyleContext" /> class.
        /// </summary>
        /// <param name="httpContext">The HTTP context.</param>
        /// <param name="writer"></param>
        /// <exception cref="System.ArgumentNullException">httpContext</exception>
        public StyleContext(HttpContextBase httpContext, TextWriter writer)
        {
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
            _writer = writer ?? throw new ArgumentNullException(nameof(writer));
            _isAjaxRequest = _httpContext.Request.IsAjaxRequest();
        }

        /// <summary>
        /// Gets or sets the resolver used to resolve paths to style files.
        /// </summary>
        /// <value>
        /// The style path resolver.
        /// </value>
        public static Func<string[], IHtmlString> StylePathResolver { get; set; } = paths =>
        {
            var builder = new StringBuilder(paths.Length);
            foreach (var path in paths)
            {
                builder.AppendLine("<link href='" + UrlHelper.GenerateContentUrl(path, Context) + "' rel='stylesheet'></link>");
            }

            return new HtmlString(builder.ToString());
        };

        /// <summary>
        /// Gets or sets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        internal static HttpContextBase Context
        {
            get { return _context ?? new HttpContextWrapper(HttpContext.Current); }
            set { _context = value; }
        }

        /// <summary>
        ///     Adds a block of style to be rendered out at a later point in the page rendering when
        ///     <see cref="StyleHtmlHelperExtensions.RenderStyles(System.Web.Mvc.HtmlHelper)" /> is called.
        /// </summary>
        /// <param name="styleBlock">the block of style to render. The block must not include the &lt;style&gt; tags</param>
        /// <param name="renderOnAjax">
        ///     if set to <c>true</c> and the request is an AJAX request, the style will be written in the response.
        /// </param>
        /// <remarks>
        ///     A call to <see cref="StyleHtmlHelperExtensions.RenderStyles(HtmlHelper)" /> will render all styles.
        /// </remarks>
        public void AddStyleBlock(string styleBlock, bool renderOnAjax = false)
        {
            if (_isAjaxRequest)
            {
                if (renderOnAjax)
                {
                    _styleBlocks.Add("<style type='text/css'>" + styleBlock + "</style>");
                }
            }
            else
            {
                _styleBlocks.Add("<style type='text/css'>" + styleBlock + "</style>");
            }
        }

        /// <summary>
        ///     Adds a block of style to be rendered out at a later point in the page rendering when
        ///     <see cref="StyleHtmlHelperExtensions.RenderStyles(System.Web.Mvc.HtmlHelper)" /> is called.
        /// </summary>
        /// <param name="styleTemplate">
        ///     the template for the block of style to render. The template must include the &lt;style
        ///     &gt; tags
        /// </param>
        /// <param name="renderOnAjax">
        ///     if set to <c>true</c> and the request is an AJAX request, the style will be written in the response.
        /// </param>
        /// <remarks>
        ///     A call to <see cref="StyleHtmlHelperExtensions.RenderStyles(HtmlHelper)" /> will render all styles.
        /// </remarks>
        public void AddStyleBlock(Func<dynamic, HelperResult> styleTemplate, bool renderOnAjax = false)
        {
            if (_isAjaxRequest)
            {
                if (renderOnAjax)
                {
                    _styleBlocks.Add(styleTemplate(null).ToString());
                }
            }
            else
            {
                _styleBlocks.Add(styleTemplate(null).ToString());
            }
        }

        /// <summary>
        ///     Adds a style file to be rendered out at a later point in the page rendering when
        ///     <see cref="StyleHtmlHelperExtensions.RenderStyles(System.Web.Mvc.HtmlHelper)" /> is called.
        /// </summary>
        /// <param name="path">the path to the style file to render later</param>
        /// <param name="renderOnAjax">
        ///     if set to <c>true</c> and the request is an AJAX request, the style will be written in the response.
        /// </param>
        /// <remarks>
        ///     A call to <see cref="StyleHtmlHelperExtensions.RenderStyles(HtmlHelper)" /> will render all styles.
        /// </remarks>
        public void AddStyleFile(string path, bool renderOnAjax = false)
        {
            if (_isAjaxRequest)
            {
                if (renderOnAjax)
                {
                    _styleFiles.Add(path);
                }
            }
            else
            {
                _styleFiles.Add(path);
            }
        }

        /// <summary>
        ///     Pushes the <see cref="StyleContext" /> onto the stack in <see cref="HttpContext.Items" />
        /// </summary>
        public void Dispose()
        {
            if (_isAjaxRequest)
            {
                var builder = new StringBuilder(_styleFiles.Count + _styleBlocks.Count);
                builder.Append(StylePathResolver(_styleFiles.ToArray()));

                foreach (var styleBlock in _styleBlocks)
                {
                    builder.AppendLine(styleBlock);
                }

                _writer.Write(builder.ToString());
                return;
            }

            var items = _httpContext.Items;
            var styleContexts = items[StyleContextItems] as Stack<StyleContext> ?? new Stack<StyleContext>();

            // remove any style files already the same as the ones we're about to add
            foreach (var styleContext in styleContexts)
            {
                styleContext._styleFiles.ExceptWith(_styleFiles);
            }

            styleContexts.Push(this);
            items[StyleContextItems] = styleContexts;
        }
    }
}
