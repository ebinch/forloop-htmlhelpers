﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

namespace Forloop.HtmlHelpers
{
    /// <summary>
    ///     Methods for helping to manage styles in partials and templates.
    /// </summary>
    public static class StyleHtmlHelperExtensions
    {
        /// <summary>
        ///     Adds a block of style to be rendered out at a later point in the page rendering when
        ///     <see cref="StyleHtmlHelperExtensions.RenderStyles(System.Web.Mvc.HtmlHelper)" /> is called.
        /// </summary>
        /// <param name="htmlHelper">the <see cref="HtmlHelper" /></param>
        /// <param name="styleBlock">the block of style to render. The block must not include the &lt;style&gt; tags</param>
        /// <param name="renderOnAjax">
        ///     if set to <c>true</c> and the request is an AJAX request, the style will be written in the response.
        /// </param>
        /// <remarks>
        ///     A call to <see cref="RenderStyles(HtmlHelper)" /> will render all styles.
        /// </remarks>
        public static void AddStyleBlock(this HtmlHelper htmlHelper, string styleBlock, bool renderOnAjax = false)
        {
            AddToStyleContext(htmlHelper, context => context.AddStyleBlock(styleBlock, renderOnAjax));
        }

        /// <summary>
        ///     Adds a block of style to be rendered out at a later point in the page rendering when
        ///     <see cref="RenderStyles(System.Web.Mvc.HtmlHelper)" /> is called.
        /// </summary>
        /// <param name="htmlHelper">the <see cref="HtmlHelper" /></param>
        /// <param name="styleTemplate">
        ///     the template for the block of style to render. The template must include the &lt;style
        ///     &gt; tags
        /// </param>
        /// <param name="renderOnAjax">
        ///     if set to <c>true</c> and the request is an AJAX request, the style will be written in the response.
        /// </param>
        /// <remarks>
        ///     A call to <see cref="RenderStyles(HtmlHelper)" /> will render all styles.
        /// </remarks>
        public static void AddStyleBlock(this HtmlHelper htmlHelper, Func<dynamic, HelperResult> styleTemplate, bool renderOnAjax = false)
        {
            AddToStyleContext(htmlHelper, context => context.AddStyleBlock(styleTemplate, renderOnAjax));
        }

        /// <summary>
        ///     Adds a style file to be rendered out at a later point in the page rendering when
        ///     <see cref="StyleHtmlHelperExtensions.RenderStyles(System.Web.Mvc.HtmlHelper)" /> is called.
        /// </summary>
        /// <param name="htmlHelper">the <see cref="HtmlHelper" /></param>
        /// <param name="path">the path to the style file to render later</param>
        /// <param name="renderOnAjax">
        ///     if set to <c>true</c> and the request is an AJAX request, the style will be written in the response.
        /// </param>
        /// <remarks>
        ///     A call to <see cref="RenderStyles(HtmlHelper)" /> will render all styles.
        /// </remarks>
        public static void AddStyleFile(this HtmlHelper htmlHelper, string path, bool renderOnAjax = false)
        {
            AddToStyleContext(htmlHelper, context => context.AddStyleFile(path, renderOnAjax));
        }

        /// <summary>
        ///     Begins a new <see cref="StyleContext" />. Used to signal that the styles inside the
        ///     context belong in the same view, partial view or template
        /// </summary>
        /// <param name="htmlHelper">
        ///     the <see cref="HtmlHelper" />
        /// </param>
        /// <returns>
        ///     a new instance of <see cref="StyleContext" />
        /// </returns>
        public static StyleContext BeginStyleContext(this HtmlHelper htmlHelper)
        {
            var context = htmlHelper.ViewContext.HttpContext;
            var styleContext = new StyleContext(context, htmlHelper.ViewContext.Writer);
            context.Items[StyleContext.StyleContextItem] = styleContext;
            return styleContext;
        }

        /// <summary>
        ///     Ends a <see cref="StyleContext" />.
        /// </summary>
        /// <param name="htmlHelper">
        ///     the <see cref="HtmlHelper" />
        /// </param>
        public static void EndStyleContext(this HtmlHelper htmlHelper)
        {
            var context = htmlHelper.ViewContext.HttpContext;
            var styleContext = context.Items[StyleContext.StyleContextItem] as StyleContext;

            if (styleContext != null)
            {
                styleContext.Dispose();
            }
        }

        /// <summary>
        ///     Renders the styles out into the view using <see cref="UrlHelper.Content" />
        ///     to generate the paths in the &lt;style&gt; elements for the style files
        /// </summary>
        /// <param name="htmlHelper">
        ///     the <see cref="HtmlHelper" />
        /// </param>
        /// <returns>
        ///     an <see cref="IHtmlString" /> of all of the styles.
        /// </returns>
        public static IHtmlString RenderStyles(this HtmlHelper htmlHelper)
        {
            return RenderStyles(htmlHelper, StyleContext.StylePathResolver);
        }

        /// <summary>
        ///     Renders the styles out into the view using the passed <paramref name="stylePathResolver" /> function
        ///     to generate the &lt;style&gt; elements for the style files.
        /// </summary>
        /// <param name="htmlHelper">
        ///     the <see cref="HtmlHelper" />
        /// </param>
        /// <param name="stylePathResolver">
        ///     a function that is passed the style paths and is used to generate the markup for
        ///     the style elements
        /// </param>
        /// <returns>
        ///     an <see cref="IHtmlString" /> of all of the styles.
        /// </returns>
        [Obsolete("Set the stylePathResolver using the StyleContext.StylePathResolver static property.", false)]
        public static IHtmlString RenderStyles(this HtmlHelper htmlHelper, Func<string[], IHtmlString> stylePathResolver)
        {
            var styleContexts =
                htmlHelper.ViewContext.HttpContext.Items[StyleContext.StyleContextItems] as Stack<StyleContext>;

            if (styleContexts != null)
            {
                var count = styleContexts.Count;
                var builder = new StringBuilder(styleContexts.Count);
                var style = new List<string>(styleContexts.Count);

                for (int i = 0; i < count; i++)
                {
                    var styleContext = styleContexts.Pop();

                    builder.Append(stylePathResolver(styleContext._styleFiles.ToArray()).ToString());
                    style.InsertRange(0, styleContext._styleBlocks);

                    // render out all the styles in one block on the last loop iteration
                    if (i == count - 1 && style.Any())
                    {
                        foreach (var s in style)
                        {
                            builder.AppendLine(s);
                        }
                    }
                }

                return new HtmlString(builder.ToString());
            }

            return MvcHtmlString.Empty;
        }

        /// <summary>
        ///     Performs an action on the current <see cref="StyleContext" />
        /// </summary>
        /// <param name="htmlHelper">
        ///     the <see cref="HtmlHelper" />
        /// </param>
        /// <param name="action">the action to perform</param>
        private static void AddToStyleContext(HtmlHelper htmlHelper, Action<StyleContext> action)
        {
            var styleContext =
                htmlHelper.ViewContext.HttpContext.Items[StyleContext.StyleContextItem] as StyleContext;

            if (styleContext == null)
            {
                throw new InvalidOperationException(
                    "No StyleContext in HttpContext.Items. Call Html.BeginStyleContext() to create a StyleContext.");
            }

            action(styleContext);
        }
    }
}